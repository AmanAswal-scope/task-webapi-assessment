﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TaskDbContext:DbContext 
    {
        public TaskDbContext()
        {

        }

        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options) { }

        public DbSet<Task2> Tasks { get; set; }

        public DbSet<SubTask> SubTasks { get; set; }

        public DbSet<User>Users { get; set; }
    }
  

}
