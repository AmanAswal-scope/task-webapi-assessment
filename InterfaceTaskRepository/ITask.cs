﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ITask
    {
        //Declare all following functions only for CRUD for Task & SubTask

        // Add a Task
        // Get all Tasks
        // Get a particular Task
        // Edit some Task
        // Delete some task, in case there is no subtask

        // Add a SubTack for a Task
        // Get all subtasks for a particular Task
        // Here in display Task Name, SubtTask Name,Created By, When


        public void AddTask(Task2 task);




        public List<Task2> GetTasks();
        public Task2 GetTaskById(int id);
        public void EditTask(int id, Task2 task);
        public void DeleteTask(int id);
        public List<SubTask> GetSubTasksByTaskId(int id);





    }
}
